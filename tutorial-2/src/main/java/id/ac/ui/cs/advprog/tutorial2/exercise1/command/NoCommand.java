package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

public class NoCommand implements Command {

    @Override
    public void execute() { }

    @Override
    public void undo() { }
}
